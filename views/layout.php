<?php
include_once "header.php";

include_once "heading.php";

?>
<div class="container">
<?php
if (is_array($content)) {
    foreach($content as $c) {
        include_once($c);
    }
} else {
    if ($content != '') {
        include_once($content);
    }
}
?>
</div>
<?php

include_once "footer.php";