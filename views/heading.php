<div class="navbar navbar-default">
    <div class="container">
        <a href='/' class='navbar-brand'>Quantox</a> 

        <ul class="nav navbar-nav navbar-right">
            <?php if ($loggedIn === false) { ?>
            <li><a href="/login">Login</a></li>
            <li><a href="/register">Register</a></li>
            <?php } else { ?>
            <li><a href="/logout">Logout</a></li>
            <?php } ?>
        </ul>
    </div>
</div>