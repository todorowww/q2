<div class="row">
    <div class="col-lg-offset-2 col-lg-10">
        <h2>Registration</h2>
    </div>
    <div class="col-lg-offset-2 col-lg-8 register">
        <form action="/register" method="POST">
            <div class="row">
                <div class="form-group">
                    <label for="regUserName" class="col-md-4 control-label">Name *</label>
                    <div class="col-md-8">
                        <input id="regUserName" type="text" class="form-control" name="regUserName" value="" required autofocus>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="c" class="col-md-4 control-label">Email *</label>
                    <div class="col-md-8">
                        <input id="regUserEmail" type="email" class="form-control" name="regUserEmail" value="" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="regUserPass" class="col-md-4 control-label">Password *</label>
                    <div class="col-md-8">
                        <input id="regUserPass" type="password" class="form-control <?php echo $errors['regUserPass'] ? 'has-errors' : ''; ?>" name="regUserPass" value="" required>
                        <?php if ($errors['regUserPass']) :?>
                        <span class="help-block">
                                <?= $messages['regUserPass'] ?>
                        </span>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="regUserPassAgain" class="col-md-4 control-label">Confirm password *</label>
                    <div class="col-md-8">
                        <input id="regUserPassAgain" type="password" class="form-control <?php echo $errors['regUserPassAgain'] ? 'has-errors' : ''; ?>" name="regUserPassAgain" value="" required>
                        <?php if ($errors['regUserPassAgain']) :?>
                        <span class="help-block">
                                <?= $messages['regUserPassAgain'] ?>
                        </span>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="row pull-right register-button">
                <input type="submit" name="register" value="Register">
            </div>
        </form>
    </div>
</div>