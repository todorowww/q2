<?php if ($errors['query']) :?>
<div class="alert-danger">
        <?= $messages['query'] ?>
</div>
<?php endif; ?>

<div class="row">
    <div class="col-lg-offset-1 col-lg-10 home">
        <h1>Senior PHP Backend Developer Test</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-offset-1 col-lg-10 search">
        <form action="/search" method="POST">
            <input type="text" name="query" size="50" class="orm-control <?php echo $errors['query'] ? 'has-errors' : ''; ?>" required autofocus>
            <input type="submit" name="search" value="Search">
        </form>
    </div>
</div>