<?php if (isset($loginMessage)) : ?>
<div class="alert-info">
    <?= $loginMessage ?>
</div>
<?php  endif; ?>
<div class="row">
    <div class="col-lg-offset-3 col-lg-6 login">
        <form action="/login" method="POST">
            <div class="row">
                <div class="form-group">
                    <label for="loginEmail" class="col-md-4 control-label">Email</label>
                    <div class="col-md-8">
                        <input id="loginEmail" type="text" class="form-control" name="loginEmail" value="" required autofocus>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="loginPass" class="col-md-4 control-label">Password</label>
                    <div class="col-md-8">
                        <input id="loginPass" type="password" class="form-control" name="loginPass" value="" required>
                    </div>
                </div>
            </div>
            <div class="row pull-right login-button">
                <input type="submit" name="login" value="Login">
            </div>
        </form>
    </div>
</div>