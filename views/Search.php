<div class="row">
    <div class="col-lg-offset-2 col-lg-10">
        <h2>Search results</h2>
    </div>
</div>
<?php
if (!empty($results)) {
    foreach ($results as $item) {
?>
<div class="row table">
    <div class="col-lg-6">
        <?= $item['user_name'] ?>
    </div>
    <div class="col-lg-6">
        <?= $item['user_email'] ?>
    </div>
</div>

<?php
    }
} else { ?>
<div class="row">
    <div class="col-lg-offset-2 col-lg-10">
        No results found
    </div>
</div>
<?php } ?>
