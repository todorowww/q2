<?php

use \todorowww\DB;
use \todorowww\Input;
use \todorowww\Router;

require_once 'vendor/autoload.php';

$route = Router::requestUri();
$DB = new DB("localhost", "quantox", "quantox", "quantox");

$errors = [];
$messages = [];

if (isset($_COOKIE['quantoxtest'])) {
    $cookie = $_COOKIE['quantoxtest'];
    $query = "SELECT * FROM `sessions` WHERE `session_id` = ?;";
    $params = [
        $cookie
    ];
    $res = $DB->execute($query, $params);
    $sess = $res->fetch(PDO::FETCH_OBJ);
    if ($sess->session_id == $cookie) {
        $loggedIn = true;
    }
} else {
    $loggedIn = false;
}

// *** Handling GET
if (Router::getMethod() === 'GET') {
    if ($route[0] === '') {
        $content = ['Home.php'];
    }
    if (strtolower($route[0]) == 'logout') {
        setcookie("quantoxtest", null, time() - 3600);
        $loggedIn = false;
        $content = ['Home.php'];
    }
    if (strtolower($route[0]) == 'login') {
        $content = ['Login.php'];
    }
    if (strtolower($route[0]) == 'register') {
        $content = ['Register.php'];
    }

}
// *** Handling POST
elseif (Router::getMethod() === 'POST') {
    if (strtolower($route[0]) == 'register') {
        $content = ['Register.php'];
        if (Input::post("regUserPass") !== Input::post("regUserPassAgain")) {
            $errors = [
                'regUserPass'      => true,
                'regUserPassAgain' => true
            ];
            $messages = [
                'regUserPass'      => 'Passwords must match.',
                'regUserPassAgain' => 'Passwords must match.'
            ];
        } elseif (Input::post('regUserName') == '') {
            $errors['regUserName'] = true;
            $messages['regUserName'] = 'Name is required.';
        } elseif (Input::post('regUserEmail') == '') {
            $errors['regUserEmail'] = true;
            $messages['regUserEmail'] = 'Email is required.';
        } else {
            $query = "INSERT INTO users (`user_name`, `user_email`, `user_password`) VALUES(?, ?, ?);";
            $params = [
                Input::post('regUserName'),
                Input::post('regUserEmail'),
                password_hash(Input::post('regUserPass'), PASSWORD_BCRYPT),
            ];
            $DB->execute($query, $params);
            $content = ['Success.php'];
            $msgSuccess = 'User was successfully registered.';
        }
    }

    if (strtolower($route[0]) == 'login') {
        $content = ['Login.php'];
        if (Input::post('loginEmail') == '') {
            $errors['loginEmail'] = true;
            $messages['loginEmail'] = 'Email is required.';
        } elseif (Input::post('loginPass') == '') {
            $errors['loginPass'] = true;
            $messages['loginPass'] = 'Password is required.';
        } else {
            $query = 'SELECT * FROM `users` WHERE `user_email` = ?;';
            $params = [
                Input::post('loginEmail')
            ];
            $res = $DB->execute($query, $params);
            $user = $res->fetch(PDO::FETCH_OBJ);
            if (password_verify(Input::post('loginPass'), $user->user_password)) {
                $msgSuccess = "Welcome, {$user->user_name}!";
                $content = ['Success.php'];
                $id = sha1(time() . $_SERVER['REMOTE_ADDR']);
                $query = "INSERT INTO `sessions` (`session_id`, `session_data`) VALUES(?, ?);";
                $user->user_password = null;
                $params = [
                    $id,
                    json_encode($user)
                ];
                $DB->execute($query, $params);
                setcookie("quantoxtest", $id, time() + 3600);
                $loggedIn = true;
            } else {
                setcookie("quantoxtest", null, time() - 3600);
            }
        }
    }

    if (strtolower($route[0]) == 'search') {
        if ($loggedIn) {
            $s = Input::post('query');
            if (strlen($s) >= 3) {
                $query = "SELECT `user_name`, `user_email` FROM `users` WHERE `user_name` LIKE ? OR `user_email` LIKE ?;";
                $params = [
                    "%$s%",
                    "%$s%",
                ];
                $r = $DB->execute($query, $params);
                $results = $r->fetchAll();
                $content = ['Search.php'];
            } else {
                $content = ['Home.php'];
                $errors['query'] = true;
                $messages['query'] = 'Query must not be blank, and contain at least 3 characters.';
            }
        } else {
            $content = ['Login.php'];
            $loginMessage = "Please, log in to use search feature.";
        }
    }
}

require_once 'views/layout.php';
