<?php

namespace todorowww;

Class Input {

    /**
     *  Returns cleaned input from GET method
     *
     * @param string $name Variable to fetch
     * @return mixed
     */
    public static function get($name) {
        if (isset($_GET[$name])) {
            return  filter_input(INPUT_GET, $name, FILTER_DEFAULT);
        }
    }

    /**
     *  Returns cleaned input from POST method
     *
     * @param string $name Variable to fetch
     * @return mixed
     */
    public static function post($name) {
        if (isset($_POST[$name])) {
            return  filter_input(INPUT_POST, $name, FILTER_DEFAULT);
        }

    }
}