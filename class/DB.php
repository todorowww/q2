<?php

namespace todorowww;

use \PDO;

Class DB {
    /**
     *
     * @var \PDO
     */
    private $db;

    public function __construct($host, $user, $pass, $db) {
        $this->db = new PDO("mysql:host=$host;dbname=$db", $user, $pass);
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * Executes specified query
     * 
     * @param string $query
     * @param string $params
     * @return \PDOStatement
     */
    public function execute($query, $params) {
        $statement = $this->db->prepare($query);
        $statement->execute($params);
        return $statement;
    }
}