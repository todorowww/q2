<?php

namespace todorowww;

Class Router {

    /**
     * Returns request URI, split into items
     * @return array
     */
    public static function requestUri() {
        $path = substr($_SERVER['REQUEST_URI'], 1);
        $split = explode('/', $path);
        return $split;
    }

    /**
     * Returns request method
     *
     * @return string
     */
    public static function getMethod() {
        return $_SERVER['REQUEST_METHOD'];
    }
}